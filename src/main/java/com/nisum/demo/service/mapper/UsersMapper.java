package com.nisum.demo.service.mapper;

import com.nisum.demo.domain.*;
import com.nisum.demo.service.dto.PhoneDTO;
import com.nisum.demo.service.dto.UsersDTO;

import org.mapstruct.*;

import java.util.List;
import java.util.UUID;

/**
 * Mapper for the entity {@link Users} and its DTO {@link UsersDTO}.
 */
@Mapper(componentModel = "spring", uses = PhoneMapper.class)
public interface UsersMapper extends EntityMapper<UsersDTO, Users> {


    @Mappings({
        @Mapping(target = "removePhones", ignore = true),
        @Mapping(target = "phones", source = "phones")
    })
    Users toEntity(UsersDTO usersDTO);

    @Mappings({
        @Mapping(target = "phones", source = "phones")
    })
    UsersDTO toDto(UsersDTO usersDTO);

    default Users fromId(UUID id) {
        if (id == null) {
            return null;
        }
        Users users = new Users();
        users.setId(id);
        return users;
    }
}
