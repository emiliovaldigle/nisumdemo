package com.nisum.demo.web.rest;

import com.nisum.demo.service.PhoneService;
import com.nisum.demo.service.UsersService;
import com.nisum.demo.web.rest.errors.BadRequestAlertException;
import com.nisum.demo.service.dto.UsersDTO;
import com.nisum.demo.service.dto.UsersCriteria;
import com.nisum.demo.service.UsersQueryService;

import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * REST controller for managing {@link com.nisum.demo.domain.Users}.
 */
@RestController
@RequestMapping({"/api"})
public class UsersResource {

    private final Logger log = LoggerFactory.getLogger(UsersResource.class);

    private static final String ENTITY_NAME = "demoUsers";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UsersService usersService;

    private final UsersQueryService usersQueryService;

    private final PhoneService phoneService;

    public UsersResource(UsersService usersService, UsersQueryService usersQueryService, PhoneService phoneService) {
        this.usersService = usersService;
        this.usersQueryService = usersQueryService;
        this.phoneService = phoneService;
    }

    /**
     * {@code POST  /users} : Create a new users.
     *
     * @param usersDTO the usersDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new usersDTO, or with status {@code 400 (Bad Request)} if the users has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/users")
    public ResponseEntity<UsersDTO> createUsers(@Valid @RequestBody UsersDTO usersDTO) throws URISyntaxException {
        log.info("REST request to save Users : {}", usersDTO);
        if (usersDTO.getId() == null)
            usersDTO.setId(UUID.randomUUID());
        Optional<UsersDTO> user = usersService.findOne(usersDTO.getId());
        if (user.isPresent())
            throw new BadRequestAlertException("Id already exists", ENTITY_NAME, "idexists");

        UsersCriteria usersCriteria = new UsersCriteria();
        StringFilter emailFilter = new StringFilter();
        emailFilter.setEquals(usersDTO.getEmail());
        usersCriteria.setEmail(emailFilter);
        List<UsersDTO> users = usersQueryService.findByCriteria(usersCriteria);
        if (users.size() > 0)
            throw new BadRequestAlertException("Email is used by another account, please enter a unique Email", ENTITY_NAME, "emailExists");

        UsersDTO result = usersService.save(usersDTO);
        ResponseEntity<UsersDTO> responseEntity = ResponseEntity.created(new URI("/api/users/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
        log.info("REST response to save Users : {} was: {}", usersDTO, responseEntity.toString());

        return responseEntity;
    }

    /**
     * {@code PUT  /users} : Updates an existing users.
     *
     * @param usersDTO the usersDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated usersDTO,
     * or with status {@code 400 (Bad Request)} if the usersDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the usersDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/users")
    public ResponseEntity<UsersDTO> updateUsers(@Valid @RequestBody UsersDTO usersDTO) throws URISyntaxException {
        log.info("REST request to update Users : {}", usersDTO);
        if (usersDTO.getId() == null)
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        if (!usersService.findOne(usersDTO.getId()).isPresent())
            throw new BadRequestAlertException("Entity does not exists", ENTITY_NAME, "entity_not_exists");

        UsersDTO result = usersService.save(usersDTO);
        ResponseEntity<UsersDTO> responseEntity = ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, usersDTO.getId().toString()))
            .body(result);
        log.info("REST response to update Users : {} was: {}", usersDTO, responseEntity.toString());

        return responseEntity;

    }

    /**
     * {@code GET  /users} : get all the users.
     *

     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of users in body.
     */
    @GetMapping("/users")
    public ResponseEntity<List<UsersDTO>> getAllUsers(UsersCriteria criteria) {
        log.info("REST request to get Users by criteria: {}", criteria);
        List<UsersDTO> entityList = usersQueryService.findByCriteria(criteria);
        ResponseEntity<List<UsersDTO>> responseEntity = ResponseEntity.ok().body(entityList);
        log.info("REST response to count Users by criteria: {} was: {}", criteria, responseEntity.toString());
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * {@code GET  /users/count} : count all the users.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/users/count")
    public ResponseEntity<Long> countUsers(UsersCriteria criteria) {
        log.info("REST request to count Users by criteria: {}", criteria);
        ResponseEntity<Long> responseEntity =  ResponseEntity.ok().body(usersQueryService.countByCriteria(criteria));
        log.info("REST response to count Users by criteria: {} was: {}", criteria, responseEntity.toString());
        return responseEntity;
    }

    /**
     * {@code GET  /users/:id} : get the "id" users.
     *
     * @param id the id of the usersDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the usersDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/users/{id}")
    public ResponseEntity<UsersDTO> getUsers(@PathVariable UUID id) {
        log.info("REST request to get Users : {}", id);
        Optional<UsersDTO> usersDTO = usersService.findOne(id);
        ResponseEntity<UsersDTO> responseEntity = ResponseUtil.wrapOrNotFound(usersDTO);
        log.info("REST response from get Users with id : {}  was: {}", id, responseEntity.toString());
        return responseEntity;
    }

    /**
     * {@code DELETE  /users/:id} : delete the "id" users.
     *
     * @param id the id of the usersDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/users/{id}")
    public ResponseEntity<Void> deleteUsers(@PathVariable UUID id) {
        log.info("REST request to delete Users : {}", id);
        phoneService.deleteByUsersId(id);
        usersService.delete(id);
        ResponseEntity<Void> responseEntity = ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
        log.info("Rest response from deleting Users Id: {} was: {}", id, responseEntity.toString());
        return responseEntity;
    }
}
