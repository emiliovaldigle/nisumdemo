package com.nisum.demo.web.rest;

import com.nisum.demo.DemoApp;
import com.nisum.demo.domain.Phone;
import com.nisum.demo.domain.Users;
import com.nisum.demo.repository.PhoneRepository;
import com.nisum.demo.repository.UsersRepository;
import com.nisum.demo.service.PhoneService;
import com.nisum.demo.service.dto.PhoneDTO;
import com.nisum.demo.service.mapper.PhoneMapper;
import com.nisum.demo.web.rest.errors.ExceptionTranslator;
import com.nisum.demo.service.dto.PhoneCriteria;
import com.nisum.demo.service.PhoneQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;

import static com.nisum.demo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PhoneResource} REST controller.
 */
@SpringBootTest(classes = DemoApp.class)
public class PhoneResourceIT {

    private static final Integer DEFAULT_NUMBER = 1;
    private static final Integer UPDATED_NUMBER = 2;
    private static final Integer SMALLER_NUMBER = 1 - 1;

    private static final Integer DEFAULT_CITY_CODE = 1;
    private static final Integer UPDATED_CITY_CODE = 2;
    private static final Integer SMALLER_CITY_CODE = 1 - 1;

    private static final Integer DEFAULT_COUNTRY_CODE = 1;
    private static final Integer UPDATED_COUNTRY_CODE = 2;
    private static final Integer SMALLER_COUNTRY_CODE = 1 - 1;

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_LAST_UPD_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_UPD_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private PhoneRepository phoneRepository;

    @Autowired
    private PhoneMapper phoneMapper;

    @Autowired
    private PhoneService phoneService;

    @Autowired
    private PhoneQueryService phoneQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private Validator validator;

    private MockMvc restPhoneMockMvc;

    private Phone phone;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PhoneResource phoneResource = new PhoneResource(phoneService, phoneQueryService);
        this.restPhoneMockMvc = MockMvcBuilders.standaloneSetup(phoneResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Phone createEntity(EntityManager em) {
        Phone phone = new Phone()
            .number(DEFAULT_NUMBER)
            .cityCode(DEFAULT_CITY_CODE)
            .countryCode(DEFAULT_COUNTRY_CODE);
        return phone;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Phone createUpdatedEntity(EntityManager em) {
        Phone phone = new Phone()
            .number(UPDATED_NUMBER)
            .cityCode(UPDATED_CITY_CODE)
            .countryCode(UPDATED_COUNTRY_CODE);
        return phone;
    }

    @BeforeEach
    public void initTest() {
        phone = createEntity(em);
    }

    @Test
    @Transactional
    public void createPhone() throws Exception {
        int databaseSizeBeforeCreate = phoneRepository.findAll().size();
        Users users = UsersResourceIT.createEntity(em);
        usersRepository.saveAndFlush(users);
        // Create the Phone
        phone.setUsers(users);
        PhoneDTO phoneDTO = phoneMapper.toDto(phone);

        restPhoneMockMvc.perform(post("/api/phones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(phoneDTO)))
            .andExpect(status().isCreated());

        // Validate the Phone in the database
        List<Phone> phoneList = phoneRepository.findAll();
        assertThat(phoneList).hasSize(databaseSizeBeforeCreate + 1);
        Phone testPhone = phoneList.get(phoneList.size() - 1);
        assertThat(testPhone.getNumber()).isEqualTo(DEFAULT_NUMBER);
        assertThat(testPhone.getCityCode()).isEqualTo(DEFAULT_CITY_CODE);
        assertThat(testPhone.getCountryCode()).isEqualTo(DEFAULT_COUNTRY_CODE);
        assertThat(testPhone.getCreatedDate()).isNotNull();
        assertThat(testPhone.getLastUpdDate()).isNotNull();
    }

    @Test
    @Transactional
    public void createPhoneWithExistingId() throws Exception {


        // Create the Phone with an existing ID
        phone = createEntity(em);
        phone = phoneRepository.saveAndFlush(phone);

        int databaseSizeBeforeCreate = phoneRepository.findAll().size();

        PhoneDTO phoneDTO = phoneMapper.toDto(phone);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPhoneMockMvc.perform(post("/api/phones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(phoneDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Phone in the database
        List<Phone> phoneList = phoneRepository.findAll();
        assertThat(phoneList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPhones() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList
        restPhoneMockMvc.perform(get("/api/phones?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").isNotEmpty())
            .andExpect(jsonPath("$.[*].number").value(hasItem(DEFAULT_NUMBER)))
            .andExpect(jsonPath("$.[*].cityCode").value(hasItem(DEFAULT_CITY_CODE)))
            .andExpect(jsonPath("$.[*].countryCode").value(hasItem(DEFAULT_COUNTRY_CODE)))
            .andExpect(jsonPath("$.[*].createdDate").doesNotExist())
            .andExpect(jsonPath("$.[*].lastUpdDate").doesNotExist());
    }

    @Test
    @Transactional
    public void getPhone() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get the phone
        restPhoneMockMvc.perform(get("/api/phones/{id}", phone.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").isNotEmpty())
            .andExpect(jsonPath("$.number").value(DEFAULT_NUMBER))
            .andExpect(jsonPath("$.cityCode").value(DEFAULT_CITY_CODE))
            .andExpect(jsonPath("$.countryCode").value(DEFAULT_COUNTRY_CODE))
            .andExpect(jsonPath("$.createdDate").doesNotExist())
            .andExpect(jsonPath("$.lastUpdDate").doesNotExist());
    }


    @Test
    @Transactional
    public void getPhonesByIdFiltering() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        Long id = phone.getId();

        defaultPhoneShouldBeFound("id.equals=" + id);
        defaultPhoneShouldNotBeFound("id.notEquals=" + id);

        defaultPhoneShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPhoneShouldNotBeFound("id.greaterThan=" + id);

        defaultPhoneShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPhoneShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllPhonesByNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where number equals to DEFAULT_NUMBER
        defaultPhoneShouldBeFound("number.equals=" + DEFAULT_NUMBER);

        // Get all the phoneList where number equals to UPDATED_NUMBER
        defaultPhoneShouldNotBeFound("number.equals=" + UPDATED_NUMBER);
    }

    @Test
    @Transactional
    public void getAllPhonesByNumberIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where number not equals to DEFAULT_NUMBER
        defaultPhoneShouldNotBeFound("number.notEquals=" + DEFAULT_NUMBER);

        // Get all the phoneList where number not equals to UPDATED_NUMBER
        defaultPhoneShouldBeFound("number.notEquals=" + UPDATED_NUMBER);
    }

    @Test
    @Transactional
    public void getAllPhonesByNumberIsInShouldWork() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where number in DEFAULT_NUMBER or UPDATED_NUMBER
        defaultPhoneShouldBeFound("number.in=" + DEFAULT_NUMBER + "," + UPDATED_NUMBER);

        // Get all the phoneList where number equals to UPDATED_NUMBER
        defaultPhoneShouldNotBeFound("number.in=" + UPDATED_NUMBER);
    }

    @Test
    @Transactional
    public void getAllPhonesByNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where number is not null
        defaultPhoneShouldBeFound("number.specified=true");

        // Get all the phoneList where number is null
        defaultPhoneShouldNotBeFound("number.specified=false");
    }

    @Test
    @Transactional
    public void getAllPhonesByNumberIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where number is greater than or equal to DEFAULT_NUMBER
        defaultPhoneShouldBeFound("number.greaterThanOrEqual=" + DEFAULT_NUMBER);

        // Get all the phoneList where number is greater than or equal to UPDATED_NUMBER
        defaultPhoneShouldNotBeFound("number.greaterThanOrEqual=" + UPDATED_NUMBER);
    }

    @Test
    @Transactional
    public void getAllPhonesByNumberIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where number is less than or equal to DEFAULT_NUMBER
        defaultPhoneShouldBeFound("number.lessThanOrEqual=" + DEFAULT_NUMBER);

        // Get all the phoneList where number is less than or equal to SMALLER_NUMBER
        defaultPhoneShouldNotBeFound("number.lessThanOrEqual=" + SMALLER_NUMBER);
    }

    @Test
    @Transactional
    public void getAllPhonesByNumberIsLessThanSomething() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where number is less than DEFAULT_NUMBER
        defaultPhoneShouldNotBeFound("number.lessThan=" + DEFAULT_NUMBER);

        // Get all the phoneList where number is less than UPDATED_NUMBER
        defaultPhoneShouldBeFound("number.lessThan=" + UPDATED_NUMBER);
    }

    @Test
    @Transactional
    public void getAllPhonesByNumberIsGreaterThanSomething() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where number is greater than DEFAULT_NUMBER
        defaultPhoneShouldNotBeFound("number.greaterThan=" + DEFAULT_NUMBER);

        // Get all the phoneList where number is greater than SMALLER_NUMBER
        defaultPhoneShouldBeFound("number.greaterThan=" + SMALLER_NUMBER);
    }


    @Test
    @Transactional
    public void getAllPhonesByCityCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where cityCode equals to DEFAULT_CITY_CODE
        defaultPhoneShouldBeFound("cityCode.equals=" + DEFAULT_CITY_CODE);

        // Get all the phoneList where cityCode equals to UPDATED_CITY_CODE
        defaultPhoneShouldNotBeFound("cityCode.equals=" + UPDATED_CITY_CODE);
    }

    @Test
    @Transactional
    public void getAllPhonesByCityCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where cityCode not equals to DEFAULT_CITY_CODE
        defaultPhoneShouldNotBeFound("cityCode.notEquals=" + DEFAULT_CITY_CODE);

        // Get all the phoneList where cityCode not equals to UPDATED_CITY_CODE
        defaultPhoneShouldBeFound("cityCode.notEquals=" + UPDATED_CITY_CODE);
    }

    @Test
    @Transactional
    public void getAllPhonesByCityCodeIsInShouldWork() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where cityCode in DEFAULT_CITY_CODE or UPDATED_CITY_CODE
        defaultPhoneShouldBeFound("cityCode.in=" + DEFAULT_CITY_CODE + "," + UPDATED_CITY_CODE);

        // Get all the phoneList where cityCode equals to UPDATED_CITY_CODE
        defaultPhoneShouldNotBeFound("cityCode.in=" + UPDATED_CITY_CODE);
    }

    @Test
    @Transactional
    public void getAllPhonesByCityCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where cityCode is not null
        defaultPhoneShouldBeFound("cityCode.specified=true");

        // Get all the phoneList where cityCode is null
        defaultPhoneShouldNotBeFound("cityCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllPhonesByCityCodeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where cityCode is greater than or equal to DEFAULT_CITY_CODE
        defaultPhoneShouldBeFound("cityCode.greaterThanOrEqual=" + DEFAULT_CITY_CODE);

        // Get all the phoneList where cityCode is greater than or equal to UPDATED_CITY_CODE
        defaultPhoneShouldNotBeFound("cityCode.greaterThanOrEqual=" + UPDATED_CITY_CODE);
    }

    @Test
    @Transactional
    public void getAllPhonesByCityCodeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where cityCode is less than or equal to DEFAULT_CITY_CODE
        defaultPhoneShouldBeFound("cityCode.lessThanOrEqual=" + DEFAULT_CITY_CODE);

        // Get all the phoneList where cityCode is less than or equal to SMALLER_CITY_CODE
        defaultPhoneShouldNotBeFound("cityCode.lessThanOrEqual=" + SMALLER_CITY_CODE);
    }

    @Test
    @Transactional
    public void getAllPhonesByCityCodeIsLessThanSomething() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where cityCode is less than DEFAULT_CITY_CODE
        defaultPhoneShouldNotBeFound("cityCode.lessThan=" + DEFAULT_CITY_CODE);

        // Get all the phoneList where cityCode is less than UPDATED_CITY_CODE
        defaultPhoneShouldBeFound("cityCode.lessThan=" + UPDATED_CITY_CODE);
    }

    @Test
    @Transactional
    public void getAllPhonesByCityCodeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where cityCode is greater than DEFAULT_CITY_CODE
        defaultPhoneShouldNotBeFound("cityCode.greaterThan=" + DEFAULT_CITY_CODE);

        // Get all the phoneList where cityCode is greater than SMALLER_CITY_CODE
        defaultPhoneShouldBeFound("cityCode.greaterThan=" + SMALLER_CITY_CODE);
    }


    @Test
    @Transactional
    public void getAllPhonesByCountryCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where countryCode equals to DEFAULT_COUNTRY_CODE
        defaultPhoneShouldBeFound("countryCode.equals=" + DEFAULT_COUNTRY_CODE);

        // Get all the phoneList where countryCode equals to UPDATED_COUNTRY_CODE
        defaultPhoneShouldNotBeFound("countryCode.equals=" + UPDATED_COUNTRY_CODE);
    }

    @Test
    @Transactional
    public void getAllPhonesByCountryCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where countryCode not equals to DEFAULT_COUNTRY_CODE
        defaultPhoneShouldNotBeFound("countryCode.notEquals=" + DEFAULT_COUNTRY_CODE);

        // Get all the phoneList where countryCode not equals to UPDATED_COUNTRY_CODE
        defaultPhoneShouldBeFound("countryCode.notEquals=" + UPDATED_COUNTRY_CODE);
    }

    @Test
    @Transactional
    public void getAllPhonesByCountryCodeIsInShouldWork() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where countryCode in DEFAULT_COUNTRY_CODE or UPDATED_COUNTRY_CODE
        defaultPhoneShouldBeFound("countryCode.in=" + DEFAULT_COUNTRY_CODE + "," + UPDATED_COUNTRY_CODE);

        // Get all the phoneList where countryCode equals to UPDATED_COUNTRY_CODE
        defaultPhoneShouldNotBeFound("countryCode.in=" + UPDATED_COUNTRY_CODE);
    }

    @Test
    @Transactional
    public void getAllPhonesByCountryCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where countryCode is not null
        defaultPhoneShouldBeFound("countryCode.specified=true");

        // Get all the phoneList where countryCode is null
        defaultPhoneShouldNotBeFound("countryCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllPhonesByCountryCodeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where countryCode is greater than or equal to DEFAULT_COUNTRY_CODE
        defaultPhoneShouldBeFound("countryCode.greaterThanOrEqual=" + DEFAULT_COUNTRY_CODE);

        // Get all the phoneList where countryCode is greater than or equal to UPDATED_COUNTRY_CODE
        defaultPhoneShouldNotBeFound("countryCode.greaterThanOrEqual=" + UPDATED_COUNTRY_CODE);
    }

    @Test
    @Transactional
    public void getAllPhonesByCountryCodeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where countryCode is less than or equal to DEFAULT_COUNTRY_CODE
        defaultPhoneShouldBeFound("countryCode.lessThanOrEqual=" + DEFAULT_COUNTRY_CODE);

        // Get all the phoneList where countryCode is less than or equal to SMALLER_COUNTRY_CODE
        defaultPhoneShouldNotBeFound("countryCode.lessThanOrEqual=" + SMALLER_COUNTRY_CODE);
    }

    @Test
    @Transactional
    public void getAllPhonesByCountryCodeIsLessThanSomething() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where countryCode is less than DEFAULT_COUNTRY_CODE
        defaultPhoneShouldNotBeFound("countryCode.lessThan=" + DEFAULT_COUNTRY_CODE);

        // Get all the phoneList where countryCode is less than UPDATED_COUNTRY_CODE
        defaultPhoneShouldBeFound("countryCode.lessThan=" + UPDATED_COUNTRY_CODE);
    }

    @Test
    @Transactional
    public void getAllPhonesByCountryCodeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where countryCode is greater than DEFAULT_COUNTRY_CODE
        defaultPhoneShouldNotBeFound("countryCode.greaterThan=" + DEFAULT_COUNTRY_CODE);

        // Get all the phoneList where countryCode is greater than SMALLER_COUNTRY_CODE
        defaultPhoneShouldBeFound("countryCode.greaterThan=" + SMALLER_COUNTRY_CODE);
    }


    @Test
    @Transactional
    public void getAllPhonesByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        phone = createEntity(em);
        phone = phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where createdDate equals to DEFAULT_CREATED_DATE
        defaultPhoneShouldBeFound("createdDate.lessThanOrEquals=" + phone.getCreatedDate());

        // Get all the phoneList where createdDate equals to UPDATED_CREATED_DATE
        defaultPhoneShouldNotBeFound("createdDate.equals=" + Instant.now().plusSeconds(10000L));
    }

    @Test
    @Transactional
    public void getAllPhonesByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultPhoneShouldBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the phoneList where createdDate not equals to UPDATED_CREATED_DATE
        defaultPhoneShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllPhonesByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        phone = createEntity(em);

        phone = phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultPhoneShouldBeFound("createdDate.in=" + phone.getCreatedDate() + "," + Instant.now());

        // Get all the phoneList where createdDate equals to UPDATED_CREATED_DATE
        defaultPhoneShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllPhonesByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where createdDate is not null
        defaultPhoneShouldBeFound("createdDate.specified=true");

        // Get all the phoneList where createdDate is null
        defaultPhoneShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllPhonesByLastUpdDateIsEqualToSomething() throws Exception {
        // Initialize the database
        phone = createEntity(em);

        phone = phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where lastUpdDate equals to DEFAULT_LAST_UPD_DATE
        defaultPhoneShouldBeFound("lastUpdDate.equals=" + phone.getLastUpdDate());

        // Get all the phoneList where lastUpdDate equals to UPDATED_LAST_UPD_DATE
        defaultPhoneShouldNotBeFound("lastUpdDate.equals=" + UPDATED_LAST_UPD_DATE);
    }

    @Test
    @Transactional
    public void getAllPhonesByLastUpdDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        phone = createEntity(em);

        phone = phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where lastUpdDate not equals to DEFAULT_LAST_UPD_DATE
        defaultPhoneShouldNotBeFound("lastUpdDate.notEquals=" + phone.getLastUpdDate());

        // Get all the phoneList where lastUpdDate not equals to UPDATED_LAST_UPD_DATE
        defaultPhoneShouldBeFound("lastUpdDate.notEquals=" + UPDATED_LAST_UPD_DATE);
    }

    @Test
    @Transactional
    public void getAllPhonesByLastUpdDateIsInShouldWork() throws Exception {
        // Initialize the database
        phone = createEntity(em);

        phone = phoneRepository.saveAndFlush(phone);

        defaultPhoneShouldNotBeFound("lastUpdDate.in=" + UPDATED_LAST_UPD_DATE);
    }

    @Test
    @Transactional
    public void getAllPhonesByLastUpdDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        // Get all the phoneList where lastUpdDate is not null
        defaultPhoneShouldBeFound("lastUpdDate.specified=true");

        // Get all the phoneList where lastUpdDate is null
        defaultPhoneShouldNotBeFound("lastUpdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllPhonesByUsersIsEqualToSomething() throws Exception {
        // Initialize the database
        Users users = UsersResourceIT.createEntity(em);
        em.persist(users);
        em.flush();
        phone.setUsers(users);
        phoneRepository.saveAndFlush(phone);
        UUID usersId = users.getId();

        // Get all the phoneList where users equals to usersId
        defaultPhoneShouldBeFound("usersId.equals=" + usersId);

    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPhoneShouldBeFound(String filter) throws Exception {
        restPhoneMockMvc.perform(get("/api/phones?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").isNotEmpty())
            .andExpect(jsonPath("$.[*].number").value(hasItem(DEFAULT_NUMBER)))
            .andExpect(jsonPath("$.[*].cityCode").value(hasItem(DEFAULT_CITY_CODE)))
            .andExpect(jsonPath("$.[*].countryCode").value(hasItem(DEFAULT_COUNTRY_CODE)))
            .andExpect(jsonPath("$.[*].createdDate").doesNotExist())
            .andExpect(jsonPath("$.[*].lastUpdDate").doesNotExist());

        // Check, that the count call also returns 1
        restPhoneMockMvc.perform(get("/api/phones/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPhoneShouldNotBeFound(String filter) throws Exception {
        restPhoneMockMvc.perform(get("/api/phones?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPhoneMockMvc.perform(get("/api/phones/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingPhone() throws Exception {
        // Get the phone
        restPhoneMockMvc.perform(get("/api/phones/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePhone() throws Exception {

        Users users = UsersResourceIT.createEntity(em);
        usersRepository.save(users);
        // Initialize the database
        phone = createUpdatedEntity(em);
        phoneRepository.save(phone);

        int databaseSizeBeforeUpdate = phoneRepository.findAll().size();

       phone = phone.number(UPDATED_NUMBER)
            .cityCode(UPDATED_CITY_CODE)
            .countryCode(UPDATED_COUNTRY_CODE)
            .createdDate(UPDATED_CREATED_DATE)
            .lastUpdDate(UPDATED_LAST_UPD_DATE);

        PhoneDTO phoneDTO = phoneMapper.toDto(phone);

        phoneDTO.setUsersId(users.getId());
        restPhoneMockMvc.perform(put("/api/phones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(phoneDTO)))
            .andExpect(status().isOk());

        // Validate the Phone in the database
        List<Phone> phoneList = phoneRepository.findAll();
        assertThat(phoneList).hasSize(databaseSizeBeforeUpdate);
        Phone testPhone = phoneList.get(phoneList.size() - 1);
        assertThat(testPhone.getNumber()).isEqualTo(UPDATED_NUMBER);
        assertThat(testPhone.getCityCode()).isEqualTo(UPDATED_CITY_CODE);
        assertThat(testPhone.getCountryCode()).isEqualTo(UPDATED_COUNTRY_CODE);

    }

    @Test
    @Transactional
    public void updateNonExistingPhone() throws Exception {
        int databaseSizeBeforeUpdate = phoneRepository.findAll().size();

        // Create the Phone
        PhoneDTO phoneDTO = phoneMapper.toDto(phone);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPhoneMockMvc.perform(put("/api/phones")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(phoneDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Phone in the database
        List<Phone> phoneList = phoneRepository.findAll();
        assertThat(phoneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePhone() throws Exception {
        // Initialize the database
        phoneRepository.saveAndFlush(phone);

        int databaseSizeBeforeDelete = phoneRepository.findAll().size();

        // Delete the phone
        restPhoneMockMvc.perform(delete("/api/phones/{id}", phone.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Phone> phoneList = phoneRepository.findAll();
        assertThat(phoneList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
