package com.nisum.demo.service.dto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.*;

/**
 * A DTO for the {@link com.nisum.demo.domain.Users} entity.
 */
@JsonPropertyOrder(
    {
        "id",
        "name",
        "email",
        "password",
        "phones"
    }
)
public class UsersDTO implements Serializable {
    private UUID id;

    @JsonProperty("name")
    private String name;

    @JsonProperty(value = "token", access = JsonProperty.Access.READ_ONLY)
    private String jwtToken;

    @JsonProperty(value = "isActive")
    private Boolean isActive = true;

    @JsonProperty(value = "lastLogin", access = JsonProperty.Access.READ_ONLY)
    private Instant lastLogin;


    @JsonProperty(value = "createdDate", access = JsonProperty.Access.READ_ONLY)
    private Instant createdDate;

    @JsonProperty(value = "lastUpdatedDate", access = JsonProperty.Access.READ_ONLY)
    private Instant lastUpdDate;

    @NotNull
    @Pattern(regexp = "^(.+)@(.+)$")
    @JsonProperty("email")
    private String email;

    @NotNull
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$")
    @JsonProperty("password")
    private String password;


    @JsonProperty("phones")
    private Set<PhoneDTO> phones;


    public Set<PhoneDTO> getPhones() {
        return phones;
    }

    public void setPhones(Set<PhoneDTO> phones) {
        this.phones = phones;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJwtToken() {
        return jwtToken;
    }

    public void setJwtToken(String jwtToken) {
        this.jwtToken = jwtToken;
    }

    public Boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Instant getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Instant lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getLastUpdDate() {
        return lastUpdDate;
    }

    public void setLastUpdDate(Instant lastUpdDate) {
        this.lastUpdDate = lastUpdDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UsersDTO usersDTO = (UsersDTO) o;
        if (usersDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), usersDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UsersDTO{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", jwtToken='" + jwtToken + '\'' +
            ", isActive=" + isActive +
            ", lastLogin=" + lastLogin +
            ", createdDate=" + createdDate +
            ", lastUpdDate=" + lastUpdDate +
            ", email='" + email + '\'' +
            ", password='" + password + '\'' +
            '}';
    }


}
