package com.nisum.demo.web.rest;


import com.nisum.demo.config.Constants;
import com.nisum.demo.service.AuthenticationService;
import com.nisum.demo.service.dto.AuthenticationDTO;
import com.nisum.demo.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Base64;
import java.util.HashMap;


@RestController
@RequestMapping({"v1/authChallenge", "/api"})
public class AuthenticationResource {
    private static final Logger log = LoggerFactory.getLogger(AuthenticationResource.class);

    private AuthenticationService authenticationService;
    public AuthenticationResource(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }


    @GetMapping(value = "/login/{rememberMe}", produces =  "application/json")
    public ResponseEntity<HashMap<String, Object>> doLogin(HttpServletRequest httpServletRequest, @PathVariable Boolean rememberMe) {
        String credentials = httpServletRequest.getHeader("Authorization");
        if (null == credentials) {
            throw new BadRequestAlertException("Header with value Authorization must be present", "Authentication", "bad_credentials");
        }
        log.info("AuthenticationResource | incoming credentials: {}", credentials);
        String decodedCredentials = new String(Base64.getDecoder().decode(credentials.substring(6)));
        HashMap<String, Object> messageWrapper;
        try{
            messageWrapper = authenticationService.authenticate(new AuthenticationDTO()
                .name(decodedCredentials.split(":")[0])
                .password(decodedCredentials.split(":")[1]), rememberMe);
        }catch (Exception ex){
            log.error("Error trying to build user specification from given input");
            throw new BadRequestAlertException(Constants.ERROR_INVALID_PAYLOAD, "Authentication", "bad_credentials");
        }
        if (null == messageWrapper || "".equals(messageWrapper.get(Constants.JWT_TOKEN)))
            throw new BadRequestAlertException("Malformed request", "Authentication", "bad_request");
        if (messageWrapper.get(Constants.MESSAGE).equals(Constants.ERROR_USER_INACTIVE))
            throw new BadRequestAlertException(Constants.ERROR_USER_INACTIVE, "Authentication", "user_inactive");
        if (messageWrapper.get(Constants.MESSAGE).equals(Constants.ERROR_MISSMATCH_CREDENTIALS))
            throw new BadRequestAlertException(Constants.ERROR_MISSMATCH_CREDENTIALS, "Authentication", "miss_match_credentials");


        ResponseEntity<HashMap<String, Object>> responseEntity =  ResponseEntity.ok(messageWrapper);
        log.info("AuthenticationResource | Response: {}",  responseEntity);
        return responseEntity;
    }
}
