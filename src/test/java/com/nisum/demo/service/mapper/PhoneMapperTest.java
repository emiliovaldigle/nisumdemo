package com.nisum.demo.service.mapper;

import com.nisum.demo.repository.PhoneRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;


import static org.assertj.core.api.Assertions.assertThat;


public class PhoneMapperTest {

    private PhoneMapper phoneMapper;

    @Autowired
    private PhoneRepository phoneRepository;

    @BeforeEach
    public void setUp() {
        phoneMapper = new PhoneMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 2L;
        assertThat(phoneMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(phoneMapper.fromId(null)).isNull();
    }

}
