package com.nisum.demo.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.nisum.demo.domain.Users} entity. This class is used
 * in {@link com.nisum.demo.web.rest.UsersResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /users?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class UsersCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private UUIDFilter id;

    private StringFilter name;

    private StringFilter jwtToken;

    private BooleanFilter isActive;

    private InstantFilter lastLogin;

    private InstantFilter createdDate;

    private InstantFilter lastUpdDate;

    private StringFilter email;

    private StringFilter password;

    private LongFilter phonesId;

    public UsersCriteria(){
    }

    public UsersCriteria(UsersCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.jwtToken = other.jwtToken == null ? null : other.jwtToken.copy();
        this.isActive = other.isActive == null ? null : other.isActive.copy();
        this.lastLogin = other.lastLogin == null ? null : other.lastLogin.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.lastUpdDate = other.lastUpdDate == null ? null : other.lastUpdDate.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.password = other.password == null ? null : other.password.copy();
        this.phonesId = other.phonesId == null ? null : other.phonesId.copy();
    }

    @Override
    public UsersCriteria copy() {
        return new UsersCriteria(this);
    }

    public UUIDFilter getId() {
        return id;
    }

    public void setId(UUIDFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getJwtToken() {
        return jwtToken;
    }

    public void setJwtToken(StringFilter jwtToken) {
        this.jwtToken = jwtToken;
    }

    public BooleanFilter getIsActive() {
        return isActive;
    }

    public void setIsActive(BooleanFilter isActive) {
        this.isActive = isActive;
    }

    public InstantFilter getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(InstantFilter lastLogin) {
        this.lastLogin = lastLogin;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public InstantFilter getLastUpdDate() {
        return lastUpdDate;
    }

    public void setLastUpdDate(InstantFilter lastUpdDate) {
        this.lastUpdDate = lastUpdDate;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public StringFilter getPassword() {
        return password;
    }

    public void setPassword(StringFilter password) {
        this.password = password;
    }

    public LongFilter getPhonesId() {
        return phonesId;
    }

    public void setPhonesId(LongFilter phonesId) {
        this.phonesId = phonesId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UsersCriteria that = (UsersCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(jwtToken, that.jwtToken) &&
            Objects.equals(isActive, that.isActive) &&
            Objects.equals(lastLogin, that.lastLogin) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(lastUpdDate, that.lastUpdDate) &&
            Objects.equals(email, that.email) &&
            Objects.equals(password, that.password) &&
            Objects.equals(phonesId, that.phonesId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        jwtToken,
        isActive,
        lastLogin,
        createdDate,
        lastUpdDate,
        email,
        password,
        phonesId
        );
    }

    @Override
    public String toString() {
        return "UsersCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (jwtToken != null ? "jwtToken=" + jwtToken + ", " : "") +
                (isActive != null ? "isActive=" + isActive + ", " : "") +
                (lastLogin != null ? "lastLogin=" + lastLogin + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (lastUpdDate != null ? "lastUpdDate=" + lastUpdDate + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (password != null ? "password=" + password + ", " : "") +
                (phonesId != null ? "phonesId=" + phonesId + ", " : "") +
            "}";
    }

}
