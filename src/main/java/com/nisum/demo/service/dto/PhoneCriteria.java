package com.nisum.demo.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.*;

/**
 * Criteria class for the {@link com.nisum.demo.domain.Phone} entity. This class is used
 * in {@link com.nisum.demo.web.rest.PhoneResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /phones?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PhoneCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter number;

    private IntegerFilter cityCode;

    private IntegerFilter countryCode;

    private InstantFilter createdDate;

    private InstantFilter lastUpdDate;

    private UUIDFilter usersId;

    public PhoneCriteria(){
    }

    public PhoneCriteria(PhoneCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.number = other.number == null ? null : other.number.copy();
        this.cityCode = other.cityCode == null ? null : other.cityCode.copy();
        this.countryCode = other.countryCode == null ? null : other.countryCode.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.lastUpdDate = other.lastUpdDate == null ? null : other.lastUpdDate.copy();
        this.usersId = other.usersId == null ? null : other.usersId.copy();
    }

    @Override
    public PhoneCriteria copy() {
        return new PhoneCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getNumber() {
        return number;
    }

    public void setNumber(IntegerFilter number) {
        this.number = number;
    }

    public IntegerFilter getCityCode() {
        return cityCode;
    }

    public void setCityCode(IntegerFilter cityCode) {
        this.cityCode = cityCode;
    }

    public IntegerFilter getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(IntegerFilter countryCode) {
        this.countryCode = countryCode;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public InstantFilter getLastUpdDate() {
        return lastUpdDate;
    }

    public void setLastUpdDate(InstantFilter lastUpdDate) {
        this.lastUpdDate = lastUpdDate;
    }

    public UUIDFilter getUsersId() {
        return usersId;
    }

    public void setUsersId(UUIDFilter usersId) {
        this.usersId = usersId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PhoneCriteria that = (PhoneCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(number, that.number) &&
            Objects.equals(cityCode, that.cityCode) &&
            Objects.equals(countryCode, that.countryCode) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(lastUpdDate, that.lastUpdDate) &&
            Objects.equals(usersId, that.usersId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        number,
        cityCode,
        countryCode,
        createdDate,
        lastUpdDate,
        usersId
        );
    }

    @Override
    public String toString() {
        return "PhoneCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (number != null ? "number=" + number + ", " : "") +
                (cityCode != null ? "cityCode=" + cityCode + ", " : "") +
                (countryCode != null ? "countryCode=" + countryCode + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (lastUpdDate != null ? "lastUpdDate=" + lastUpdDate + ", " : "") +
                (usersId != null ? "usersId=" + usersId + ", " : "") +
            "}";
    }

}
