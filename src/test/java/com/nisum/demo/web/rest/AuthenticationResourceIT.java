package com.nisum.demo.web.rest;

import com.nisum.demo.DemoApp;
import com.nisum.demo.config.Constants;
import com.nisum.demo.domain.Phone;
import com.nisum.demo.domain.Users;
import com.nisum.demo.repository.PhoneRepository;
import com.nisum.demo.repository.UsersRepository;
import com.nisum.demo.security.jwt.TokenProvider;
import com.nisum.demo.service.AuthenticationService;
import com.nisum.demo.service.PhoneQueryService;
import com.nisum.demo.service.PhoneService;
import com.nisum.demo.service.UsersService;
import com.nisum.demo.service.dto.UsersDTO;
import com.nisum.demo.service.mapper.PhoneMapper;
import com.nisum.demo.web.rest.errors.ExceptionTranslator;
import com.sun.xml.bind.v2.runtime.reflect.opt.Const;
import io.github.jhipster.config.JHipsterProperties;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;

import java.util.Base64;
import java.util.Optional;

import static com.nisum.demo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest(classes = DemoApp.class)
public class AuthenticationResourceIT {
    private static final String DEFAULT_EMAIL = "test@tester.com";


    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;


    private MockMvc restPhoneMockMvc;


    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AuthenticationResource authenticationResource = new AuthenticationResource(authenticationService);
        this.restPhoneMockMvc = MockMvcBuilders.standaloneSetup(authenticationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    @Test
    @Transactional
    public void doLoginWithSuccess() throws Exception {
        Users user = UsersResourceIT.createEntity(em);
        user.setEmail(DEFAULT_EMAIL);
        user.setIsActive(true);
        usersRepository.saveAndFlush(user);
        String credentials = user.getEmail() + ":" + user.getPassword();
        String encodedCredentials = new String(Base64.getEncoder().encode(credentials.getBytes()));

        restPhoneMockMvc.perform(get("/v1/authChallenge/login/true").header("Authorization", "Basic "+ encodedCredentials) )
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.user").value(user.getEmail()))
            .andExpect(jsonPath("$.message").value("ok"))
            .andExpect(jsonPath("$.token").isNotEmpty());

        Optional<Users> userUpdated = usersRepository.findById(user.getId());
        assertThat(userUpdated.isPresent()).isEqualTo(true);
        assertThat(userUpdated.get().getJwtToken()).isNotEmpty();
        assertThat(userUpdated.get().getLastLogin()).isNotEqualTo(user.getLastLogin());



    }

    @Test
    @Transactional
    public void doLoginWithMismatchCredentials() throws Exception {
        Users user = UsersResourceIT.createEntity(em);
        String credentials = user.getEmail() + ":" + user.getPassword();
        String encodedCredentials = new String(Base64.getEncoder().encode(credentials.getBytes()));

        restPhoneMockMvc.perform(get("/v1/authChallenge/login/true").header("Authorization", "Basic "+ encodedCredentials) )
            .andExpect(status().isBadRequest())
            .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON))
            .andExpect(jsonPath("$.message").value("error.miss_match_credentials"))
            .andExpect(jsonPath("$.token").doesNotExist());

    }

    @Test
    @Transactional
    public void doLoginWithInvalidInput() throws Exception {
        Users user = UsersResourceIT.createEntity(em);
        String credentials = user.getEmail() + ":" + user.getPassword();

        restPhoneMockMvc.perform(get("/v1/authChallenge/login/true").header("Authorization", "Basic "+ credentials) )
            .andExpect(status().isInternalServerError())
            .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON))
            .andExpect(jsonPath("$.message").value("error.http.500"))
            .andExpect(jsonPath("$.token").doesNotExist());
    }
    @Test
    @Transactional
    public void doLoginWithInactiveUser() throws Exception {
        Users user = UsersResourceIT.createEntity(em);
        user.setEmail(DEFAULT_EMAIL);
        user.setIsActive(false);
        usersRepository.saveAndFlush(user);
        String credentials = user.getEmail() + ":" + user.getPassword();
        String encodedCredentials = new String(Base64.getEncoder().encode(credentials.getBytes()));

        restPhoneMockMvc.perform(get("/v1/authChallenge/login/true").header("Authorization", "Basic "+ encodedCredentials))
            .andExpect(status().isBadRequest())
            .andExpect(content().contentType(MediaType.APPLICATION_PROBLEM_JSON))
            .andExpect(jsonPath("$.message").value("error.user_inactive"))
            .andExpect(jsonPath("$.token").doesNotExist());
    }
}
