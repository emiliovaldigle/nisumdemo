package com.nisum.demo.web.rest;

import com.nisum.demo.DemoApp;
import com.nisum.demo.domain.Users;
import com.nisum.demo.domain.Phone;
import com.nisum.demo.repository.UsersRepository;
import com.nisum.demo.service.PhoneService;
import com.nisum.demo.service.UsersService;
import com.nisum.demo.service.dto.UsersDTO;
import com.nisum.demo.service.mapper.UsersMapper;
import com.nisum.demo.web.rest.errors.ExceptionTranslator;
import com.nisum.demo.service.UsersQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import static com.nisum.demo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link UsersResource} REST controller.
 */
@SpringBootTest(classes = DemoApp.class)
public class UsersResourceIT {
    private static final UUID UPDATED_ID = UUID.randomUUID();

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_JWT_TOKEN = "AAAAAAAAAA";
    private static final String UPDATED_JWT_TOKEN = "BBBBBBBBBB";

    private static final Boolean DEFAULT_IS_ACTIVE = false;
    private static final Boolean UPDATED_IS_ACTIVE = true;

    private static final Instant DEFAULT_LAST_LOGIN = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_LOGIN = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant UPDATED_LAST_UPD_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_EMAIL = "AAAAA@eeee.com";
    private static final String UPDATED_EMAIL = "eeee@eeee.com";

    private static final String DEFAULT_PASSWORD = "Xe6w_521D3257mfaeEFVC";
    private static final String UPDATED_PASSWORD = "]>VnS]3156795xE:Lhm";

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private UsersMapper usersMapper;

    @Autowired
    private UsersService usersService;

    @Autowired
    private UsersQueryService usersQueryService;

    @Autowired
    private PhoneService phoneService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restUsersMockMvc;

    private Users users;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UsersResource usersResource = new UsersResource(usersService, usersQueryService, phoneService);
        this.restUsersMockMvc = MockMvcBuilders.standaloneSetup(usersResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Users createEntity(EntityManager em) {
        Random rnd = new Random();
        UUID id = UUID.randomUUID();
        char c = (char) (rnd.nextInt(26) + 'a');
        int rand_int = rnd.nextInt(1000);
        Users users = new Users()
            .id(id)
            .name(DEFAULT_NAME)
            .jwtToken(DEFAULT_JWT_TOKEN)
            .isActive(DEFAULT_IS_ACTIVE)
            .lastLogin(DEFAULT_LAST_LOGIN)
            .email(c + rand_int + DEFAULT_EMAIL)
            .password(DEFAULT_PASSWORD);
        Phone phone = PhoneResourceIT.createEntity(em);
        phone.setUsers(users);
        users.addPhones(phone);
        return users;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Users createUpdatedEntity(EntityManager em) {
        Users users = new Users()
            .id(UPDATED_ID)
            .name(UPDATED_NAME)
            .jwtToken(UPDATED_JWT_TOKEN)
            .isActive(UPDATED_IS_ACTIVE)
            .lastLogin(UPDATED_LAST_LOGIN)
            .email(UPDATED_EMAIL)
            .password(UPDATED_PASSWORD);

        Phone phone = PhoneResourceIT.createEntity(em);
        phone.setUsers(users);
        users.addPhones(phone);
        return users;
    }

    @BeforeEach
    public void initTest() {
        users = createEntity(em);
    }

    @Test
    @Transactional
    public void createUsers() throws Exception {
        int databaseSizeBeforeCreate = usersRepository.findAll().size();

        users = createEntity(em);

        // Create the Users
        UsersDTO usersDTO = usersMapper.toDto(users);

        restUsersMockMvc.perform(post("/api/users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usersDTO)))
            .andExpect(status().isCreated());

        // Validate the Users in the database
        List<Users> usersList = usersRepository.findAll();
        assertThat(usersList).hasSize(databaseSizeBeforeCreate + 1);
        Users testUsers = usersList.get(usersList.size() - 1);
        assertThat(testUsers.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testUsers.getJwtToken()).isNull();
        assertThat(testUsers.isIsActive()).isEqualTo(DEFAULT_IS_ACTIVE);
        assertThat(testUsers.getLastLogin()).isNull();
        assertThat(testUsers.getCreatedDate()).isNotNull();
        assertThat(testUsers.getLastUpdDate()).isNotNull();
        assertThat(testUsers.getEmail()).isEqualTo(usersDTO.getEmail());
        assertThat(testUsers.getPassword()).isEqualTo(DEFAULT_PASSWORD);
    }

    @Test
    @Transactional
    public void createUsersWithExistingId() throws Exception {
        // Create the Users with an existing ID
        users = usersRepository.saveAndFlush(users);
        int databaseSizeBeforeCreate = usersRepository.findAll().size();
        UsersDTO usersDTO = usersMapper.toDto(users);
        // An entity with an existing ID cannot be created, so this API call must fail
        restUsersMockMvc.perform(post("/api/users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usersDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Users in the database
        List<Users> usersList = usersRepository.findAll();
        assertThat(usersList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = usersRepository.findAll().size();
        // set the field null
        users.setEmail(null);

        // Create the Users, which fails.
        UsersDTO usersDTO = usersMapper.toDto(users);

        restUsersMockMvc.perform(post("/api/users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usersDTO)))
            .andExpect(status().isBadRequest());

        List<Users> usersList = usersRepository.findAll();
        assertThat(usersList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPasswordIsRequired() throws Exception {
        int databaseSizeBeforeTest = usersRepository.findAll().size();
        // set the field null
        users.setPassword(null);

        // Create the Users, which fails.
        UsersDTO usersDTO = usersMapper.toDto(users);

        restUsersMockMvc.perform(post("/api/users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usersDTO)))
            .andExpect(status().isBadRequest());

        List<Users> usersList = usersRepository.findAll();
        assertThat(usersList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllUsers() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList
        restUsersMockMvc.perform(get("/api/users?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").isNotEmpty())
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].token").value(hasItem(DEFAULT_JWT_TOKEN)))
            .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].lastLogin").value(hasItem(DEFAULT_LAST_LOGIN.toString())))
            .andExpect(jsonPath("$.[*].createdDate").isNotEmpty())
            .andExpect(jsonPath("$.[*].lastUpdatedDate").isNotEmpty())
            .andExpect(jsonPath("$.[*].email").isNotEmpty())
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD)));
    }

    @Test
    @Transactional
    public void getUsers() throws Exception {
        // Initialize the database
        users = usersRepository.saveAndFlush(createEntity(em));

        // Get the users
        restUsersMockMvc.perform(get("/api/users/{id}", users.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").isNotEmpty())
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.token").value(DEFAULT_JWT_TOKEN))
            .andExpect(jsonPath("$.isActive").value(DEFAULT_IS_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.lastLogin").value(DEFAULT_LAST_LOGIN.toString()))
            .andExpect(jsonPath("$.lastUpdatedDate").isNotEmpty())
            .andExpect(jsonPath("$.email").isNotEmpty())
            .andExpect(jsonPath("$.password").value(DEFAULT_PASSWORD));
    }


    @Test
    @Transactional
    public void getUsersByIdFiltering() throws Exception {
        // Initialize the database
        users = usersRepository.saveAndFlush(createEntity(em));

        UUID id = users.getId();

        defaultUsersShouldBeFound("id.equals=" + id);
        defaultUsersShouldNotBeFound("id.notEquals=" + id);

        defaultUsersShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultUsersShouldBeFound("id.greaterThan=" + id);

        defaultUsersShouldBeFound("id.lessThanOrEqual=" + id);
        defaultUsersShouldNotBeFound("id.notEquals=" + id);
    }


    @Test
    @Transactional
    public void getAllUsersByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where name equals to DEFAULT_NAME
        defaultUsersShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the usersList where name equals to UPDATED_NAME
        defaultUsersShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllUsersByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where name not equals to DEFAULT_NAME
        defaultUsersShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the usersList where name not equals to UPDATED_NAME
        defaultUsersShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllUsersByNameIsInShouldWork() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where name in DEFAULT_NAME or UPDATED_NAME
        defaultUsersShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the usersList where name equals to UPDATED_NAME
        defaultUsersShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllUsersByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where name is not null
        defaultUsersShouldBeFound("name.specified=true");

        // Get all the usersList where name is null
        defaultUsersShouldNotBeFound("name.specified=false");
    }
    @Test
    @Transactional
    public void getAllUsersByNameContainsSomething() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where name contains DEFAULT_NAME
        defaultUsersShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the usersList where name contains UPDATED_NAME
        defaultUsersShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllUsersByNameNotContainsSomething() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where name does not contain DEFAULT_NAME
        defaultUsersShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the usersList where name does not contain UPDATED_NAME
        defaultUsersShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }


    @Test
    @Transactional
    public void getAllUsersByJwtTokenIsEqualToSomething() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where jwtToken equals to DEFAULT_JWT_TOKEN
        defaultUsersShouldBeFound("jwtToken.equals=" + DEFAULT_JWT_TOKEN);

        // Get all the usersList where jwtToken equals to UPDATED_JWT_TOKEN
        defaultUsersShouldNotBeFound("jwtToken.equals=" + UPDATED_JWT_TOKEN);
    }

    @Test
    @Transactional
    public void getAllUsersByJwtTokenIsNotEqualToSomething() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where jwtToken not equals to DEFAULT_JWT_TOKEN
        defaultUsersShouldNotBeFound("jwtToken.notEquals=" + DEFAULT_JWT_TOKEN);

        // Get all the usersList where jwtToken not equals to UPDATED_JWT_TOKEN
        defaultUsersShouldBeFound("jwtToken.notEquals=" + UPDATED_JWT_TOKEN);
    }

    @Test
    @Transactional
    public void getAllUsersByJwtTokenIsInShouldWork() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where jwtToken in DEFAULT_JWT_TOKEN or UPDATED_JWT_TOKEN
        defaultUsersShouldBeFound("jwtToken.in=" + DEFAULT_JWT_TOKEN + "," + UPDATED_JWT_TOKEN);

        // Get all the usersList where jwtToken equals to UPDATED_JWT_TOKEN
        defaultUsersShouldNotBeFound("jwtToken.in=" + UPDATED_JWT_TOKEN);
    }

    @Test
    @Transactional
    public void getAllUsersByJwtTokenIsNullOrNotNull() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where jwtToken is not null
        defaultUsersShouldBeFound("jwtToken.specified=true");

        // Get all the usersList where jwtToken is null
        defaultUsersShouldNotBeFound("jwtToken.specified=false");
    }
    @Test
    @Transactional
    public void getAllUsersByJwtTokenContainsSomething() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where jwtToken contains DEFAULT_JWT_TOKEN
        defaultUsersShouldBeFound("jwtToken.contains=" + DEFAULT_JWT_TOKEN);

        // Get all the usersList where jwtToken contains UPDATED_JWT_TOKEN
        defaultUsersShouldNotBeFound("jwtToken.contains=" + UPDATED_JWT_TOKEN);
    }

    @Test
    @Transactional
    public void getAllUsersByJwtTokenNotContainsSomething() throws Exception {
        // Initialize the database
         usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where jwtToken does not contain DEFAULT_JWT_TOKEN
        defaultUsersShouldNotBeFound("jwtToken.doesNotContain=" + DEFAULT_JWT_TOKEN);

        // Get all the usersList where jwtToken does not contain UPDATED_JWT_TOKEN
        defaultUsersShouldBeFound("jwtToken.doesNotContain=" + UPDATED_JWT_TOKEN);
    }


    @Test
    @Transactional
    public void getAllUsersByIsActiveIsEqualToSomething() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where isActive equals to DEFAULT_IS_ACTIVE
        defaultUsersShouldBeFound("isActive.equals=" + DEFAULT_IS_ACTIVE);

        // Get all the usersList where isActive equals to UPDATED_IS_ACTIVE
        defaultUsersShouldNotBeFound("isActive.equals=" + UPDATED_IS_ACTIVE);
    }

    @Test
    @Transactional
    public void getAllUsersByIsActiveIsNotEqualToSomething() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where isActive not equals to DEFAULT_IS_ACTIVE
        defaultUsersShouldNotBeFound("isActive.notEquals=" + DEFAULT_IS_ACTIVE);

        // Get all the usersList where isActive not equals to UPDATED_IS_ACTIVE
        defaultUsersShouldBeFound("isActive.notEquals=" + UPDATED_IS_ACTIVE);
    }

    @Test
    @Transactional
    public void getAllUsersByIsActiveIsInShouldWork() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where isActive in DEFAULT_IS_ACTIVE or UPDATED_IS_ACTIVE
        defaultUsersShouldBeFound("isActive.in=" + DEFAULT_IS_ACTIVE + "," + UPDATED_IS_ACTIVE);

        // Get all the usersList where isActive equals to UPDATED_IS_ACTIVE
        defaultUsersShouldNotBeFound("isActive.in=" + UPDATED_IS_ACTIVE);
    }

    @Test
    @Transactional
    public void getAllUsersByIsActiveIsNullOrNotNull() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where isActive is not null
        defaultUsersShouldBeFound("isActive.specified=true");

        // Get all the usersList where isActive is null
        defaultUsersShouldNotBeFound("isActive.specified=false");
    }

    @Test
    @Transactional
    public void getAllUsersByLastLoginIsEqualToSomething() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where lastLogin equals to DEFAULT_LAST_LOGIN
        defaultUsersShouldBeFound("lastLogin.equals=" + DEFAULT_LAST_LOGIN);

        // Get all the usersList where lastLogin equals to UPDATED_LAST_LOGIN
        defaultUsersShouldNotBeFound("lastLogin.equals=" + UPDATED_LAST_LOGIN);
    }

    @Test
    @Transactional
    public void getAllUsersByLastLoginIsNotEqualToSomething() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where lastLogin not equals to DEFAULT_LAST_LOGIN
        defaultUsersShouldNotBeFound("lastLogin.notEquals=" + DEFAULT_LAST_LOGIN);

        // Get all the usersList where lastLogin not equals to UPDATED_LAST_LOGIN
        defaultUsersShouldBeFound("lastLogin.notEquals=" + UPDATED_LAST_LOGIN);
    }

    @Test
    @Transactional
    public void getAllUsersByLastLoginIsInShouldWork() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where lastLogin in DEFAULT_LAST_LOGIN or UPDATED_LAST_LOGIN
        defaultUsersShouldBeFound("lastLogin.in=" + DEFAULT_LAST_LOGIN + "," + UPDATED_LAST_LOGIN);

        // Get all the usersList where lastLogin equals to UPDATED_LAST_LOGIN
        defaultUsersShouldNotBeFound("lastLogin.in=" + UPDATED_LAST_LOGIN);
    }

    @Test
    @Transactional
    public void getAllUsersByLastLoginIsNullOrNotNull() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where lastLogin is not null
        defaultUsersShouldBeFound("lastLogin.specified=true");

        // Get all the usersList where lastLogin is null
        defaultUsersShouldNotBeFound("lastLogin.specified=false");
    }

    @Test
    @Transactional
    public void getAllUsersByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        users = usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where createdDate equals to DEFAULT_CREATED_DATE
        defaultUsersShouldBeFound("createdDate.lessThanOrEquals=" + users.getCreatedDate());

        // Get all the usersList where createdDate equals to UPDATED_CREATED_DATE
        defaultUsersShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllUsersByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultUsersShouldBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the usersList where createdDate not equals to UPDATED_CREATED_DATE
        defaultUsersShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllUsersByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        users = usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultUsersShouldBeFound("createdDate.lessThanOrEquals=" + users.getCreatedDate());

        // Get all the usersList where createdDate equals to UPDATED_CREATED_DATE
        defaultUsersShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllUsersByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where createdDate is not null
        defaultUsersShouldBeFound("createdDate.specified=true");

        // Get all the usersList where createdDate is null
        defaultUsersShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllUsersByLastUpdDateIsEqualToSomething() throws Exception {
        // Initialize the database
        users =  usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where lastUpdDate equals to DEFAULT_LAST_UPD_DATE

        // Get all the usersList where lastUpdDate equals to UPDATED_LAST_UPD_DATE
        defaultUsersShouldNotBeFound("lastUpdDate.equals=" + UPDATED_LAST_UPD_DATE);
    }

    @Test
    @Transactional
    public void getAllUsersByLastUpdDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where lastUpdDate not equals to UPDATED_LAST_UPD_DATE
        defaultUsersShouldBeFound("lastUpdDate.notEquals=" + UPDATED_LAST_UPD_DATE);
    }

    @Test
    @Transactional
    public void getAllUsersByLastUpdDateIsInShouldWork() throws Exception {
        // Initialize the database
        users = usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where lastUpdDate in DEFAULT_LAST_UPD_DATE or UPDATED_LAST_UPD_DATE
        defaultUsersShouldBeFound("lastUpdDate.lessThanOrEqual=" + users.getLastUpdDate());

        // Get all the usersList where lastUpdDate equals to UPDATED_LAST_UPD_DATE
        defaultUsersShouldNotBeFound("lastUpdDate.in=" + UPDATED_LAST_UPD_DATE);
    }

    @Test
    @Transactional
    public void getAllUsersByLastUpdDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where lastUpdDate is not null
        defaultUsersShouldBeFound("lastUpdDate.specified=true");

        // Get all the usersList where lastUpdDate is null
        defaultUsersShouldNotBeFound("lastUpdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllUsersByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        users = usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where email equals to DEFAULT_EMAIL
        defaultUsersShouldBeFound("email.equals=" + users.getEmail());

        // Get all the usersList where email equals to UPDATED_EMAIL
        defaultUsersShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllUsersByEmailIsNotEqualToSomething() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where email not equals to UPDATED_EMAIL
        defaultUsersShouldBeFound("email.notEquals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllUsersByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        users =  usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultUsersShouldBeFound("email.in=" + users.getEmail());

        // Get all the usersList where email equals to UPDATED_EMAIL
        defaultUsersShouldNotBeFound("email.in=" + UPDATED_EMAIL + "d90kj4s");
    }

    @Test
    @Transactional
    public void getAllUsersByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where email is not null
        defaultUsersShouldBeFound("email.specified=true");

        // Get all the usersList where email is null
        defaultUsersShouldNotBeFound("email.specified=false");
    }
    @Test
    @Transactional
    public void getAllUsersByEmailContainsSomething() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where email contains DEFAULT_EMAIL
        defaultUsersShouldBeFound("email.contains=" + DEFAULT_EMAIL);

        // Get all the usersList where email contains UPDATED_EMAIL
        defaultUsersShouldNotBeFound("email.contains=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllUsersByEmailNotContainsSomething() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where email does not contain DEFAULT_EMAIL
        defaultUsersShouldNotBeFound("email.doesNotContain=" + DEFAULT_EMAIL);

    }


    @Test
    @Transactional
    public void getAllUsersByPasswordIsEqualToSomething() throws Exception {
        // Initialize the database
        users = createEntity(em);
        users = usersRepository.saveAndFlush(users);

        // Get all the usersList where password equals to DEFAULT_PASSWORD
        defaultUsersShouldBeFound("password.equals=" + users.getPassword());

        // Get all the usersList where password equals to UPDATED_PASSWORD
        defaultUsersShouldNotBeFound("password.equals=" + UPDATED_PASSWORD);
    }

    @Test
    @Transactional
    public void getAllUsersByPasswordIsNotEqualToSomething() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where password not equals to DEFAULT_PASSWORD
        defaultUsersShouldNotBeFound("password.notEquals=" + DEFAULT_PASSWORD);

        // Get all the usersList where password not equals to UPDATED_PASSWORD
        defaultUsersShouldBeFound("password.notEquals=" + UPDATED_PASSWORD);
    }

    @Test
    @Transactional
    public void getAllUsersByPasswordIsInShouldWork() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where password in DEFAULT_PASSWORD or UPDATED_PASSWORD
        defaultUsersShouldBeFound("password.in=" + DEFAULT_PASSWORD + "," + UPDATED_PASSWORD);

        // Get all the usersList where password equals to UPDATED_PASSWORD
        defaultUsersShouldNotBeFound("password.in=" + UPDATED_PASSWORD);
    }

    @Test
    @Transactional
    public void getAllUsersByPasswordIsNullOrNotNull() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where password is not null
        defaultUsersShouldBeFound("password.specified=true");

        // Get all the usersList where password is null
        defaultUsersShouldNotBeFound("password.specified=false");
    }
    @Test
    @Transactional
    public void getAllUsersByPasswordContainsSomething() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where password contains DEFAULT_PASSWORD
        defaultUsersShouldBeFound("password.contains=" + DEFAULT_PASSWORD);

        // Get all the usersList where password contains UPDATED_PASSWORD
        defaultUsersShouldNotBeFound("password.contains=" + UPDATED_PASSWORD);
    }

    @Test
    @Transactional
    public void getAllUsersByPasswordNotContainsSomething() throws Exception {
        // Initialize the database
        usersRepository.saveAndFlush(createEntity(em));

        // Get all the usersList where password does not contain DEFAULT_PASSWORD
        defaultUsersShouldNotBeFound("password.doesNotContain=" + DEFAULT_PASSWORD);

        // Get all the usersList where password does not contain UPDATED_PASSWORD
        defaultUsersShouldBeFound("password.doesNotContain=" + UPDATED_PASSWORD);
    }


    @Test
    @Transactional
    public void getAllUsersByPhonesIsEqualToSomething() throws Exception {
        // Initialize the database
        users = createEntity(em);
        Phone phones = PhoneResourceIT.createEntity(em);
        users.addPhones(phones);
        usersRepository.saveAndFlush(users);


        defaultUsersShouldNotBeFound("phonesId.equals=" + 999999);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultUsersShouldBeFound(String filter) throws Exception {
        restUsersMockMvc.perform(get("/api/users?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").isNotEmpty())
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].token").value(hasItem(DEFAULT_JWT_TOKEN)))
            .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].lastLogin").value(hasItem(DEFAULT_LAST_LOGIN.toString())))
            .andExpect(jsonPath("$.[*].createdDate").isNotEmpty())
            .andExpect(jsonPath("$.[*].lastUpdatedDate").isNotEmpty())
            .andExpect(jsonPath("$.[*].password").isNotEmpty());

        // Check, that the count call also returns 1
        restUsersMockMvc.perform(get("/api/users/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultUsersShouldNotBeFound(String filter) throws Exception {
        restUsersMockMvc.perform(get("/api/users?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restUsersMockMvc.perform(get("/api/users/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingUsers() throws Exception {
        // Get the users
        restUsersMockMvc.perform(get("/api/users/{id}", UUID.randomUUID()))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUsers() throws Exception {
        // Initialize the database
        users = createEntity(em);
        users = usersRepository.saveAndFlush(users);

        int databaseSizeBeforeUpdate = usersRepository.findAll().size();

        // Update the users
        Users updatedUsers = usersRepository.findById(users.getId()).get();
        // Disconnect from session so that the updates on updatedUsers are not directly saved in db
        em.detach(updatedUsers);

        updatedUsers
            .name(UPDATED_NAME)
            .email(UPDATED_EMAIL)
            .jwtToken(UPDATED_JWT_TOKEN)
            .isActive(UPDATED_IS_ACTIVE)
            .lastLogin(UPDATED_LAST_LOGIN)
            .password(UPDATED_PASSWORD);
        updatedUsers.setId(users.getId());
        UsersDTO usersDTO = usersMapper.toDto(updatedUsers);

        restUsersMockMvc.perform(put("/api/users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usersDTO)))
            .andExpect(status().isOk());

        // Validate the Users in the database
        List<Users> usersList = usersRepository.findAll();
        assertThat(usersList).hasSize(databaseSizeBeforeUpdate);
        Users testUsers = usersList.get(usersList.size() - 1);
        assertThat(testUsers.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testUsers.getJwtToken()).isNull();
        assertThat(testUsers.isIsActive()).isEqualTo(UPDATED_IS_ACTIVE);
        assertThat(testUsers.getLastLogin()).isNull();
        assertThat(testUsers.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testUsers.getPassword()).isEqualTo(UPDATED_PASSWORD);
    }

    @Test
    @Transactional
    public void updateNonExistingUsers() throws Exception {
        int databaseSizeBeforeUpdate = usersRepository.findAll().size();

        // Create the Users
        UsersDTO usersDTO = usersMapper.toDto(createEntity(em));

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUsersMockMvc.perform(put("/api/users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(usersDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Users in the database
        List<Users> usersList = usersRepository.findAll();
        assertThat(usersList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUsers() throws Exception {
        // Initialize the database
        users = usersRepository.saveAndFlush(createEntity(em));

        int databaseSizeBeforeDelete = usersRepository.findAll().size();

        // Delete the users
        restUsersMockMvc.perform(delete("/api/users/{id}", users.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Users> usersList = usersRepository.findAll();
        assertThat(usersList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
