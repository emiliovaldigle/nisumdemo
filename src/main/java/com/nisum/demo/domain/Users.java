package com.nisum.demo.domain;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * A Users.
 */
@Entity
@Table(name = "users")
@EntityListeners(AuditingEntityListener.class)

public class Users implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
   // @SequenceGenerator(name = "sequenceGenerator")
    private UUID id;

    @Column(name = "name")
    private String name;

    @Column(name = "jwt_token")
    private String jwtToken;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "last_login")
    private Instant lastLogin;

    @CreatedDate
    @Column(name = "created_date")
    private Instant createdDate;

    @LastModifiedDate
    @Column(name = "lastUpdDate")
    private Instant lastUpdDate;

    @NotNull
    @Pattern(regexp = "^(.+)@(.+)$")
    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @NotNull
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$")
    @Column(name = "password", nullable = false)
    private String password;


    @OneToMany(mappedBy = "users")
    private Set<Phone> phones = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Users id(UUID id){
        this.id = id;
        return this;
    }
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Users name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJwtToken() {
        return jwtToken;
    }

    public Users jwtToken(String jwtToken) {
        this.jwtToken = jwtToken;
        return this;
    }

    public void setJwtToken(String jwtToken) {
        this.jwtToken = jwtToken;
    }

    public Boolean isIsActive() {
        return isActive;
    }

    public Users isActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Instant getLastLogin() {
        return lastLogin;
    }

    public Users lastLogin(Instant lastLogin) {
        this.lastLogin = lastLogin;
        return this;
    }

    public void setLastLogin(Instant lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public Users createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getLastUpdDate() {
        return lastUpdDate;
    }

    public Users lastUpdDate(Instant lastUpdDate) {
        this.lastUpdDate = lastUpdDate;
        return this;
    }

    public void setLastUpdDate(Instant lastUpdDate) {
        this.lastUpdDate = lastUpdDate;
    }

    public String getEmail() {
        return email;
    }

    public Users email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public Users password(String password) {
        this.password = password;
        return this;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Phone> getPhones() {
        return phones;
    }

    public Users phones(Set<Phone> phones) {
        this.phones = phones;
        return this;
    }

    public Users addPhones(Phone phone) {
        this.phones.add(phone);
        phone.setUsers(this);
        return this;
    }

    public Users removePhones(Phone phone) {
        this.phones.remove(phone);
        phone.setUsers(null);
        return this;
    }

    public void setPhones(Set<Phone> phones) {
        this.phones = phones;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Users)) {
            return false;
        }
        return id != null && id.equals(((Users) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Users{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", jwtToken='" + getJwtToken() + "'" +
            ", isActive='" + isIsActive() + "'" +
            ", lastLogin='" + getLastLogin() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastUpdDate='" + getLastUpdDate() + "'" +
            ", email='" + getEmail() + "'" +
            ", password='" + getPassword() + "'" +
            "}";
    }
}
