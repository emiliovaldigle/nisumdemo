package com.nisum.demo.service.mapper;

import com.nisum.demo.domain.*;
import com.nisum.demo.service.dto.PhoneDTO;

import org.mapstruct.*;


/**
 * Mapper for the entity {@link Phone} and its DTO {@link PhoneDTO}.
 */
@Mapper(componentModel = "spring", uses = {UsersMapper.class})
public interface PhoneMapper extends EntityMapper<PhoneDTO, Phone> {

    @Mapping(source = "users.id", target = "usersId")
    PhoneDTO toDto(Phone phone);

    @Mapping(target = "users.id", source = "usersId")
    Phone toEntity(PhoneDTO phoneDTO);

    default Phone fromId(Long id) {
        if (id == null) {
            return null;
        }
        Phone phone = new Phone();
        phone.setId(id);
        return phone;
    }
}
