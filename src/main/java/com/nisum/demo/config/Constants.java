package com.nisum.demo.config;

/**
 * Application constants.
 */
public final class Constants {

    public static final String SYSTEM_ACCOUNT = "system";
    public static final String ERROR_USER_INACTIVE =  "User is not in 'active' state";
    public static final String ERROR_MISSMATCH_CREDENTIALS = "User/ Password won't match";
    public static final String JWT_TOKEN = "token" ;
    public static final String MESSAGE = "message";
    public static final Object OK = "ok" ;
    public static final String ERROR_INVALID_PAYLOAD = "Error with the content from Authorization header";

    private Constants() {
    }
}
