package com.nisum.demo.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.Instant;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the {@link com.nisum.demo.domain.Phone} entity.
 */
public class PhoneDTO implements Serializable {
    private Long id;

    private Integer number;

    private Integer cityCode;

    private Integer countryCode;

    @JsonIgnore
    private Instant createdDate;

    @JsonIgnore
    private Instant lastUpdDate;

    private UUID usersId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getCityCode() {
        return cityCode;
    }

    public void setCityCode(Integer cityCode) {
        this.cityCode = cityCode;
    }

    public Integer getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(Integer countryCode) {
        this.countryCode = countryCode;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getLastUpdDate() {
        return lastUpdDate;
    }

    public void setLastUpdDate(Instant lastUpdDate) {
        this.lastUpdDate = lastUpdDate;
    }

    public UUID getUsersId() {
        return usersId;
    }

    public void setUsersId(UUID usersId) {
        this.usersId = usersId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PhoneDTO phoneDTO = (PhoneDTO) o;
        if (phoneDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), phoneDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PhoneDTO{" +
            "id=" + getId() +
            ", number=" + getNumber() +
            ", cityCode=" + getCityCode() +
            ", countryCode=" + getCountryCode() +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastUpdDate='" + getLastUpdDate() + "'" +
            ", usersId=" + getUsersId() +
            "}";
    }


}
