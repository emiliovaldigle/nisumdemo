package com.nisum.demo.service;

import com.nisum.demo.domain.Users;
import com.nisum.demo.repository.PhoneRepository;
import com.nisum.demo.repository.UsersRepository;
import com.nisum.demo.service.dto.PhoneDTO;
import com.nisum.demo.service.dto.UsersDTO;
import com.nisum.demo.service.mapper.PhoneMapper;
import com.nisum.demo.service.mapper.UsersMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Users}.
 */
@Service
@Transactional
public class UsersService {

    private final Logger log = LoggerFactory.getLogger(UsersService.class);

    private final UsersRepository usersRepository;

    private final UsersMapper usersMapper;

    private final PhoneRepository phoneRepository;

    private final PhoneMapper phoneMapper;
    public UsersService(UsersRepository usersRepository, UsersMapper usersMapper, PhoneRepository phoneRepository, PhoneMapper phoneMapper) {
        this.usersRepository = usersRepository;
        this.usersMapper = usersMapper;
        this.phoneRepository = phoneRepository;
        this.phoneMapper = phoneMapper;
    }

    /**
     * Save a users.
     *
     * @param usersDTO the entity to save.
     * @return the persisted entity.
     */
    public UsersDTO save(UsersDTO usersDTO) {
        log.debug("Request to save Users : {}", usersDTO);
        Optional<Users> userExists = usersRepository.findById(usersDTO.getId());
        if (userExists.isPresent())
            usersDTO.setCreatedDate(userExists.get().getCreatedDate());
        Users users = usersMapper.toEntity(usersDTO);
        users = usersRepository.saveAndFlush(users);
        if (users.getPhones().size() > 0)  users.getPhones().removeAll(users.getPhones());
        users.getPhones().addAll(phoneMapper.toEntity(savePhones(usersDTO)));
        return usersMapper.toDto(users);
    }

    public List<PhoneDTO> savePhones(UsersDTO  usersDTO){
        List<PhoneDTO> phoneDTOS = new ArrayList<>();
        for (PhoneDTO p: usersDTO.getPhones()) {
            p.setUsersId(usersDTO.getId());
            phoneDTOS.add(phoneMapper.toDto(phoneRepository.save(phoneMapper.toEntity(p))));
        }
        return phoneDTOS;
    }
    /**
     * Get all the users.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<UsersDTO> findAll() {
        log.debug("Request to get all Users");
        return usersRepository.findAll().stream()
            .map(usersMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one users by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<UsersDTO> findOne(UUID id) {
        log.debug("Request to get Users : {}", id);
        return usersRepository.findById(id)
            .map(usersMapper::toDto);
    }

    /**
     * Delete the users by id.
     *
     * @param id the id of the entity.
     */
    public void delete(UUID id) {
        log.debug("Request to delete Users : {}", id);

        usersRepository.deleteById(id);
    }
}
