package com.nisum.demo.service;

import com.nisum.demo.config.Constants;
import com.nisum.demo.repository.UsersRepository;
import com.nisum.demo.security.jwt.TokenProvider;
import com.nisum.demo.service.dto.AuthenticationDTO;
import com.nisum.demo.service.dto.UsersCriteria;
import com.nisum.demo.service.dto.UsersDTO;
import com.nisum.demo.service.mapper.UsersMapper;
import io.github.jhipster.service.filter.StringFilter;

import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;


@Service
public class AuthenticationService {

    private TokenProvider tokenProvider;

    private UsersQueryService usersQueryService;

    private UsersRepository usersRepository;

    private UsersMapper usersMapper;

    private HashMap<String, Object> messageWrapper;

    public AuthenticationService(TokenProvider tokenProvider, UsersQueryService usersQueryService, UsersRepository usersRepository, UsersMapper userMapper) {
        this.tokenProvider = tokenProvider;
        this.usersQueryService = usersQueryService;
        this.usersRepository = usersRepository;
        this.usersMapper = userMapper;
        messageWrapper = new HashMap<>();
    }

    public HashMap<String, Object> authenticate(AuthenticationDTO authenticationDTO, Boolean rememberMe) {
        UsersCriteria usersCriteria = new UsersCriteria();
        StringFilter emailFilter = new StringFilter();
        StringFilter passwordFilter = new StringFilter();
        emailFilter.setEquals(authenticationDTO.getName());
        passwordFilter.setEquals(authenticationDTO.getPassword());
        usersCriteria.setEmail(emailFilter);
        usersCriteria.setPassword(passwordFilter);
        List<UsersDTO> users = usersQueryService.findByCriteria(usersCriteria);
        if (null != users && users.size() > 0) {
            if (users.get(0).isIsActive()) {
                String token = tokenProvider.createToken(authenticationDTO, rememberMe);
                UsersDTO updatedUser = users.get(0);
                updatedUser.setLastLogin(Instant.now());
                updatedUser.setJwtToken(token);
                usersRepository.save(usersMapper.toEntity(updatedUser));
                messageWrapper.put("user", authenticationDTO.getName());
                messageWrapper.put(Constants.JWT_TOKEN, token);
                messageWrapper.put(Constants.MESSAGE, Constants.OK);

            }else {
                messageWrapper.put(Constants.MESSAGE, Constants.ERROR_USER_INACTIVE);
            }
        }else {
            messageWrapper.put(Constants.MESSAGE, Constants.ERROR_MISSMATCH_CREDENTIALS);
        }
        return messageWrapper;
    }
}
