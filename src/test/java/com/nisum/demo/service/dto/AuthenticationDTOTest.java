package com.nisum.demo.service.dto;


import org.junit.jupiter.api.Test;


import static org.assertj.core.api.Assertions.assertThat;

public class AuthenticationDTOTest {

    @Test
    public void equalsVerifier() throws Exception {

        AuthenticationDTO authenticationDTO = new AuthenticationDTO();
        AuthenticationDTO authenticationDTO2 = new AuthenticationDTO();
        assertThat(authenticationDTO).isEqualTo(authenticationDTO2);
        authenticationDTO2.setName("echo");
        assertThat(authenticationDTO).isNotEqualTo(authenticationDTO2);
        authenticationDTO.setName(null);
        assertThat(authenticationDTO).isNotEqualTo(authenticationDTO2);
    }
}
