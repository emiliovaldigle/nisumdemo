package com.nisum.demo.service.mapper;

import com.nisum.demo.domain.Phone;
import com.nisum.demo.domain.Users;
import com.nisum.demo.repository.UsersRepository;
import com.nisum.demo.service.dto.UsersDTO;
import com.nisum.demo.web.rest.UsersResourceIT;
import io.swagger.models.auth.In;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;


public class UsersMapperTest {
    private UsersMapper usersMapper;


    @BeforeEach
    public void setUp() {
        usersMapper = new UsersMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        UUID id = UUID.randomUUID();
        assertThat(usersMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(usersMapper.fromId(null)).isNull();
    }

}
