package com.nisum.demo.service.dto;


import com.nisum.demo.security.AuthoritiesConstants;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

public class AuthenticationDTO  implements Authentication {

    private Collection<? extends GrantedAuthority> authorities;

    private Object credentials;

    private Object details;

    private Object principal;

    private Boolean isAuthenticated;

    private String  name;


    private Boolean rememberMe;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuthenticationDTO that = (AuthenticationDTO) o;
        return Objects.equals(authorities, that.authorities) &&
            Objects.equals(credentials, that.credentials) &&
            Objects.equals(details, that.details) &&
            Objects.equals(principal, that.principal) &&
            Objects.equals(isAuthenticated, that.isAuthenticated) &&
            Objects.equals(name, that.name) &&
            Objects.equals(rememberMe, that.rememberMe) &&
            Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(authorities, credentials, details, principal, isAuthenticated, name, rememberMe, password);
    }

    public void setCredentials(Object credentials) {
        this.credentials = credentials;
    }

    public void setDetails(Object details) {
        this.details = details;
    }

    public void setPrincipal(Object principal) {
        this.principal = principal;
    }

    public Boolean getRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(Boolean rememberMe) {
        this.rememberMe = rememberMe;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    private String password;

    public String getPassword() {
        return password;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(AuthoritiesConstants.ANONYMOUS));
        return authorities;
    }

    @Override
    public Object getCredentials() {
        return this.credentials;
    }

    @Override
    public Object getDetails() {
        return this.details;
    }

    @Override
    public Object getPrincipal() {
        return this.principal;
    }

    @Override
    public boolean isAuthenticated() {
        return this.isAuthenticated;
    }

    @Override
    public void setAuthenticated(boolean b) throws IllegalArgumentException {
        this.isAuthenticated = b;
    }

    @Override
    public String getName() {
        return this.name;
    }

    public AuthenticationDTO password(String password){
        this.password = password;
        return this;
    }
    public AuthenticationDTO name(String name){
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return "AuthenticationDTO{" +
            "authorities=" + authorities +
            ", credentials=" + credentials +
            ", details=" + details +
            ", principal=" + principal +
            ", isAuthenticated=" + isAuthenticated +
            ", name='" + name + '\'' +
            ", rememberMe=" + rememberMe +
            ", password='" + password + '\'' +
            '}';
    }
}
