package com.nisum.demo.repository;

import com.nisum.demo.domain.Users;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;


/**
 * Spring Data  repository for the Users entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UsersRepository extends JpaRepository<Users, Long>, JpaSpecificationExecutor<Users> {
   Optional<Users> findById(UUID id);
   void deleteById(UUID id);
}
