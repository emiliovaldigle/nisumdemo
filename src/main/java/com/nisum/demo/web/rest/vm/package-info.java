/**
 * View Models used by Spring MVC REST controllers.
 */
package com.nisum.demo.web.rest.vm;
